const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const concat = require('gulp-concat');
const browserSync = require('browser-sync');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');

gulp.task("clean", function () {
    return del(['build/index.html', 'build/styles/style.css']);
  });

gulp.task('image', function () {
    gulp.src('./src/assets/img/*')
        // .pipe(image())
        .pipe(gulp.dest('./build/assets/img'));
});

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./build/styles'));
});

gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        },
    })
});

gulp.task('autoprefixer', function () {
    return gulp.src('./build/styles/style.css')
        .pipe(autoprefixer())
        .pipe(gulp.dest('./build/styles'));
});

gulp.task('watch', ['sass', 'autoprefixer', 'html', 'image', 'browserSync'], function () {
    gulp.watch('./src/**/*.scss', ['sass']);
    gulp.watch('.src/**/*.scss', ['autoprefixer']);
    gulp.watch('./src/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['clean', 'watch', 'image']);